@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					You are logged in!
				</div>

				<div
				line-chart
				line-data='[
					@foreach ($results as $result)
						{ y: "{{$result->start_time}}", a: {{$result->download_speed}}, b: {{$result->upload_speed}} },
					@endforeach
				]'
				line-xkey='y'
				line-ykeys='["a", "b"]'
				line-labels='["Download speed", "Upload speed"]'
				line-colors='["#31C0BE", "#7a92a3"]'>
				</div>
				
			</div>
		</div>
	</div>
</div>
@endsection
