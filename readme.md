## Laravel-speedtest

First things first. Git clone this project and run the following command to install laravel 5.
```
composer install
``` 
You need to make sure you have npm installed. With this tool we can install bower and gulp.

```
npm install grunt-bower-install
npm install gulp-install
npm install
```

After you have bower installed we need to install this project dependencies.
```
bower install jquery
bower install bootstrap
bower install angular
bower install angular-morris-chart
```

To build our dependencies run the following command to build them. After a couple of seconds your operating system will give a notification laravel elixer has compiled.
```
gulp
``` 

To run this project on your development machine run
```
php artisan serve
```

To run a speedtest every thirty minutes just add this line to your cronfile.

```
* * * * * php /path/to/root/artisan schedule:run 1>> /dev/null 2>&1
```

Please keep in mind every test takes about 0.5 GB of traffic on high speed connections. 
```
user@host:~$ ifconfig 
eth0      Link encap:Ethernet 
          RX bytes:14656095555 (14.6 GB)  TX bytes:11089952252 (11.0 GB)
```
```
user@host:~$ speedtest-cli
Testing from ******* B.V. (xxx.xxx.xxx.xxx)...
Selecting best server based on ping...
Hosted by AltusHost B.V. (Amsterdam) [0.77 km]: 12.08 ms
Testing download speed........................................
Download: 897.61 Mbits/s
Testing upload speed..................................................
Upload: 274.14 Mbits/s
```
```
user@host:~$ ifconfig 
eth0      Link encap:Ethernet 
          RX bytes:15085481504 (15.0 GB)  TX bytes:11114983420 (11.1 GB)
```

Running this test 48 times a day will cost you 24 GB of traffic per day.

## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
