#!/bin/bash
wget -O speedtest-cli https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest_cli.py
chmod +x speedtest-cli
mv speedtest-cli /usr/bin/

wget -O speedtest-csv https://raw.githubusercontent.com/HenrikBengtsson/speedtest-cli-extras/master/bin/speedtest-csv
chmod +x speedtest-csv
mv speedtest-csv /usr/bin/
