var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less('app.less')
       .scripts([
	    	'../assets/bower/jquery/dist/jquery.js',
	    	'../assets/bower/bootstrap/dist/js/bootstrap.js',
	    	'../assets/bower/angular/angular.js',
	    	'../assets/bower/angular-morris-chart/src/angular-morris-chart.min.js',
	    	'../assets/bower/morris.js/morris.js',
	    	'../assets/bower/raphael/raphael.js',
		], 'public/js/vendor.js');
});
