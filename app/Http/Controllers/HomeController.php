<?php namespace App\Http\Controllers;

use App\Models\Result;
use DB;

class HomeController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		// DB::connection()->enableQueryLog();

		// retrieve latest records
		$date_filter = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' - 1 month'));
		
		$results = Result::orderBy('start_time')
			->where('start_time', '>=', $date_filter)
			->select('start_time', 'download_speed', 'upload_speed')
			->get();

		// dd(DB::getQueryLog());

		foreach($results as $key => &$result) {
			$result->download_speed = preg_replace("/[^0-9,.]/", "", $result->download_speed) ?: FALSE;
			$result->upload_speed = preg_replace("/[^0-9,.]/", "", $result->upload_speed) ?: FALSE;
			
			if (!$result->download_speed || !$result->upload_speed) {
				unset($results[$key]);
			}
		}

		return view('home', compact('results'));
	}

}
