<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model {

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'results';


  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['start_time','end_time','provider','ip_address','target_hostname',
    'target_distance','target_ping','download_speed','upload_speed','image_url'];

}
