<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Result;

class ResultCreator extends Command {

	private $maxretry = 10;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'run-speedtest';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This command will run a speedtest and saves the result';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// this will get called by laravel
		$this->test();
	}

	/**
	 * This function runs the speedtest and saves the result
	 *
	 * @return Result
	 */
	private function test($retry = 0)
	{
		if ( ! $outputRaw = shell_exec('/usr/bin/speedtest-csv'))
		{
			throw new NoOutputException;
		}

		$output = $this->createCleanInput(explode(';', $outputRaw));

		// check if this output is valid
		if (!$this->validateOutput($output)) 
		{
			 // should we retry?
			if ($retry <= $this->maxretry) 
			{
				$this->test($retry + 1);
			}

			throw new Exception('No valid output after ' . $retry . ' time(s).');
		}

		$result = Result::create($output);

		dd($outputRaw, $output, $result);
	}

	private function validateOutput(array $output)
	{
		if ($output['download_speed'] && $output['upload_speed'] ) 
		{
			// Good news everyone!
			return TRUE;
		}

		// we have no speedtest result and that's bad news.
		return FALSE;
	}

	private function createCleanInput(array $output)
	{
		$keys = ['start_time','end_time','provider','ip_address','target_hostname','target_distance','target_ping','download_speed','upload_speed','image_url'];

		return array_combine($keys, $output);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}
}
