<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('results', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('start_time');
			$table->string('end_time');
			$table->string('provider');
			$table->string('ip_address');
			$table->string('target_hostname');
			$table->string('target_distance');
			$table->string('target_ping');
			$table->string('download_speed');
			$table->string('upload_speed');
			$table->string('image_url');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('results');
	}

}
