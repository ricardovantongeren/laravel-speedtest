<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateResultsTable extends Migration {

	public function up()
	{
	    DB::statement('ALTER TABLE results MODIFY COLUMN start_time DATETIME');
	    DB::statement('ALTER TABLE results MODIFY COLUMN end_time DATETIME');
	}

	public function down()
	{
	    DB::statement('ALTER TABLE results MODIFY COLUMN start_time VARCHAR(255)');
	    DB::statement('ALTER TABLE results MODIFY COLUMN end_time VARCHAR(255)');
	}   


}
