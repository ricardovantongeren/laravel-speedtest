<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
 
class UserTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('users')->delete();
 
        User::create(array(
 
            'name'          => 'Ricardo van Tongeren',
            'email'         => 'ricardovantongeren@gmail.com',
            'password'      => Hash::make('password') //hashes our password nicely for us
 
        ));
 
    }
 
}